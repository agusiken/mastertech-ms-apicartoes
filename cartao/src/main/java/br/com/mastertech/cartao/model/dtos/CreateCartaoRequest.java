package br.com.mastertech.cartao.model.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;

public class CreateCartaoRequest {
    @JsonProperty("numero")
    private String number;

    @NotNull
    @JsonProperty("clienteId")
    private Long customerId;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

}
