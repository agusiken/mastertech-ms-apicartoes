package br.com.mastertech.cartao.model.dtos;

import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.model.Cliente;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao toCartao(CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(createCartaoRequest.getNumber());

        Cliente cliente = new Cliente();
        cliente.setId(createCartaoRequest.getCustomerId());
        cartao.setClienteId(cliente.getId());

        return cartao;
    }

    public CreateCartaoResponse toCreateCartaoResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumber(cartao.getNumero());
        createCartaoResponse.setCustomerId(cartao.getClienteId());
        createCartaoResponse.setActive(cartao.isAtivo());

        return createCartaoResponse;
    }

    public Cartao toCartao(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();

        cartao.setNumero(updateCartaoRequest.getNumber());
        cartao.setAtivo(updateCartaoRequest.isAtivo());

        return cartao;
    }

    public UpdateCartaoResponse toUpdateCartaoResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumber(cartao.getNumero());
        updateCartaoResponse.setCustomerId(cartao.getClienteId());
        updateCartaoResponse.setAtivo(cartao.isAtivo());

        return updateCartaoResponse;
    }

    public GetCartaoResponse toGetCartaoResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumber(cartao.getNumero());
        getCartaoResponse.setCustomerId(cartao.getClienteId());
        getCartaoResponse.setActive(cartao.isAtivo());

        return getCartaoResponse;
    }

}
