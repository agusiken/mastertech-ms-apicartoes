package br.com.mastertech.cartao.controller;

import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.model.dtos.*;
import br.com.mastertech.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoMapper mapper;

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    public CreateCartaoResponse postCartao(@RequestBody CreateCartaoRequest createCartaoRequest) {
        Cartao cartao = mapper.toCartao(createCartaoRequest);

        Cartao cartaoObjeto = cartaoService.salvarCartao(cartao);

        return mapper.toCreateCartaoResponse(cartaoObjeto);
    }

    @PatchMapping("/{numero}")
    public UpdateCartaoResponse patchAtivarCartao(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest) {

        updateCartaoRequest.setNumber(numero);

        Cartao cartao = mapper.toCartao(updateCartaoRequest);

        try {
            cartao = cartaoService.ativarCartao(cartao);

            return mapper.toUpdateCartaoResponse(cartao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse getCartaoByNumber(@PathVariable(name = "numero") String numero) {
        Cartao cartao = cartaoService.consultarPorNumero(numero);
        return mapper.toGetCartaoResponse(cartao);
    }

    @GetMapping("/id/{id}")
    public GetCartaoResponse findById(@PathVariable Long id) {
        Cartao cartao = cartaoService.consultarPorId(id);
        return mapper.toGetCartaoResponse(cartao);
    }

}