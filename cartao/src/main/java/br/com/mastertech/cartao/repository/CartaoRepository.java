package br.com.mastertech.cartao.repository;

import br.com.mastertech.cartao.model.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {
    Optional<Cartao> findByNumero(String numero);
    boolean existsByNumero(String numero);
}
