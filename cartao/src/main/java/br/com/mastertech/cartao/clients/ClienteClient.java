package br.com.mastertech.cartao.clients;

import br.com.mastertech.cartao.model.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Cliente consultarClientePorId(@PathVariable Long id);


}
