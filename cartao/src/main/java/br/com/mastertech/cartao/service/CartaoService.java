package br.com.mastertech.cartao.service;

import br.com.mastertech.cartao.clients.ClienteClient;
import br.com.mastertech.cartao.exceptions.CartaoNotFoundException;
import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.model.Cliente;
import br.com.mastertech.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public Cartao salvarCartao(Cartao cartao) {
        Cliente clienteObjeto = clienteClient.consultarClientePorId(cartao.getClienteId());

        cartao.setClienteId(clienteObjeto.getId());
        cartao.setAtivo(false);

        Cartao cartaoObjeto = cartaoRepository.save(cartao);

        return cartaoObjeto;
    }

    public Cartao ativarCartao(Cartao cartao) {
        Cartao cartaoResponse = consultarPorNumero(cartao.getNumero());

        cartaoResponse.setClienteId(cartao.getClienteId());

        cartaoResponse.setAtivo(cartao.isAtivo());

        Cartao cartaoObjeto = cartaoRepository.save(cartaoResponse);

        return cartaoObjeto;
    }

    public Cartao consultarPorNumero(String numero) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        if (cartaoOptional.isPresent()) {
            return cartaoOptional.get();
        }

        throw new CartaoNotFoundException();
    }

    public Cartao consultarPorId(Long id){
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if (byId.isPresent()) {
            return byId.get();
        }

        throw new CartaoNotFoundException();
    }

}
