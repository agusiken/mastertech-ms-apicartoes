package br.com.mastertech.pagamento.controller;

import br.com.mastertech.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {
    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<?> postPagamento(@RequestBody Pagamento pagamento) {
        return ResponseEntity.status(201).body(pagamentoService.setPagamento(pagamento));
    }

    @GetMapping("/{cartaoId}")
    public ResponseEntity<?> findAllByCartaoId(@PathVariable Long cartaoId) {
        try {
            return ResponseEntity.status(200).body(pagamentoService.findAllByCartaoId(cartaoId));
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

}
