package br.com.mastertech.pagamento.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new CartaoClientDecoder();
    }

}
