package br.com.mastertech.pagamento.service;

import br.com.mastertech.pagamento.clients.CartaoClient;
import br.com.mastertech.pagamento.model.Cartao;
import br.com.mastertech.pagamento.model.Pagamento;
import br.com.mastertech.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento setPagamento(Pagamento pagamento) {

        Cartao cartao = cartaoClient.findById(pagamento.getCartaoId());
        pagamento.setCartaoId(cartao.getId());

        return pagamentoRepository.save(pagamento);

    }

    public List<Pagamento> findAllByCartaoId(Long cartaoId) {
        try {
            return pagamentoRepository.findAllByCartaoId(cartaoId);
        } catch (RuntimeException ex) {
            throw new RuntimeException("Cartão não encontrado.");
        }
    }
}
