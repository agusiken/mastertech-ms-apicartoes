package br.com.mastertech.cliente.cliente.controller;

import br.com.mastertech.cliente.cliente.model.Cliente;
import br.com.mastertech.cliente.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    public ResponseEntity<Cliente> setCliente(@RequestBody Cliente cliente) {
        System.out.println("Post Request: " + System.currentTimeMillis());
        Cliente clienteObjeto = clienteService.setCliente(cliente);

        return ResponseEntity.status(201).body(clienteObjeto);
    }

    @GetMapping
    public Iterable<Cliente> getClientes() {
        System.out.println("Get Request: " + System.currentTimeMillis());
        return clienteService.getClientes();
    }

    @GetMapping("/{idCliente}")
    public Cliente getCliente(@PathVariable(name = "idCliente") Long idCliente) {
        Cliente cliente = clienteService.buscarPorId(idCliente);
        return cliente;
    }
}
