package br.com.mastertech.cliente.cliente.repository;

import br.com.mastertech.cliente.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
