package br.com.mastertech.fatura.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "pagamento", configuration = PagamentoClientConfiguration.class)
public interface PagamentoClient {

    //delete mapping para deletar lançamentos
}
