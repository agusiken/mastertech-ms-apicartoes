package br.com.mastertech.fatura.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PagamentoClientConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new PagamentoClientDecoder();
    }

}
