package br.com.mastertech.fatura.models.dtos;

public class CartaoExpiradoResponse {
    private String status;

    public CartaoExpiradoResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
